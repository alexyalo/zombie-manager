import { Express } from 'express';
import { Connection, createConnection } from 'typeorm';
import { getContext } from '../src/delivery/context';
import { makeApp } from '../src/delivery/app';

export default class TestEnvironment {
  private dbConnection: Connection;

  async getExpressApp(): Promise<Express> {
    this.dbConnection = await createConnection();
    await this.dbConnection.dropDatabase();
    await this.dbConnection.synchronize();
    await this.dbConnection.runMigrations();
    const context = await getContext(this.dbConnection);
    return makeApp(context);
  }

  async teardown(): Promise<void> {
    await this.dbConnection.close();
  }
}
