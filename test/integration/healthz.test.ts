import request from 'supertest';

import TestEnvironment from '../TestEnvironment';

describe('GET /healthz Integration Test', () => {
  it('returns a 200 OK status', async () => {
    const testEnvironment = new TestEnvironment();
    const app = await testEnvironment.getExpressApp();

    const response = await request(app).get('/healthz');

    expect(response.status).toBe(200);
  });
})