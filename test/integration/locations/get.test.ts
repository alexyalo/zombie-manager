import { Express } from 'express';
import request from 'supertest';
import TestEnvironment from '../../TestEnvironment';
import { createZombie } from '../integrationTestUtils';

const testEnvironment = new TestEnvironment();

describe('GET /locations Integration Test', () => {
  let app: Express;

  beforeEach(async () => {
    app = await testEnvironment.getExpressApp();
  });

  afterEach(async () => {
    await testEnvironment.teardown();
  });

  it('should return the existing Locations', async () => {
    const response = await request(app).get('/locations');

    const { data } = response.body;
    expect(response.status).toBe(200);
    expect(data).toHaveLength(3);
    expect(data[0].name).toBe('The Hospital');
    expect(data[0].id).toHaveLength(36);

    expect(data[1].name).toBe('The School');
    expect(data[1].id).toHaveLength(36);

    expect(data[2].name).toBe('The Warehouse');
    expect(data[2].id).toHaveLength(36);
  });

  it('should return the existing Locations with the right count of zombies', async () => {
    const locationsResponse = await request(app).get('/locations');

    await Promise.all([
      createZombie(app, {
        name: 'Zombie 1',
        locationId: locationsResponse.body.data[0].id,
      }),
      createZombie(app, {
        name: 'Zombie 2',
        locationId: locationsResponse.body.data[0].id,
      }),
      createZombie(app, {
        name: 'Zombie 3',
        locationId: locationsResponse.body.data[1].id,
      }),
    ]);

    const latestLocationsResponse = await request(app).get('/locations');

    const { data } = latestLocationsResponse.body;
    expect(latestLocationsResponse.status).toBe(200);
    expect(data).toHaveLength(3);
    expect(data[0].name).toBe('The Hospital');
    expect(data[0].id).toHaveLength(36);
    expect(data[0].zombieCount).toBe(2);

    expect(data[1].name).toBe('The School');
    expect(data[1].id).toHaveLength(36);
    expect(data[1].zombieCount).toBe(1);

    expect(data[2].name).toBe('The Warehouse');
    expect(data[2].id).toHaveLength(36);
    expect(data[2].zombieCount).toBe(0);
  });

  it('should return a different count for zombies in a location after updating the zombie location', async () => {
    const locationsResponse = await request(app).get('/locations');

    const zombieResponse = await createZombie(app, {
      name: 'Zombie 1',
      locationId: locationsResponse.body.data[0].id,
    });

    const secondLocationsResponse = await request(app).get('/locations');

    await request(app)
      .patch(`/zombies/${zombieResponse.body.id}`)
      .send({ locationId: locationsResponse.body.data[1].id })
      .set('Content-type', 'application/json');
    const latestLocationsResponse = await request(app).get('/locations');

    const { data: beforeUpdateData } = secondLocationsResponse.body;
    expect(beforeUpdateData).toHaveLength(3);
    expect(beforeUpdateData[0].zombieCount).toBe(1);
    expect(beforeUpdateData[1].zombieCount).toBe(0);
    expect(beforeUpdateData[2].zombieCount).toBe(0);

    const { data: postUpdateData } = latestLocationsResponse.body;
    expect(postUpdateData).toHaveLength(3);
    expect(postUpdateData[0].zombieCount).toBe(0);
    expect(postUpdateData[1].zombieCount).toBe(1);
    expect(postUpdateData[2].zombieCount).toBe(0);
  });
});
