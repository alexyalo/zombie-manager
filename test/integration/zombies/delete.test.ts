import { Express, Response } from 'express';
import request from 'supertest';
import { LocationResponse } from '../../../src/delivery/responses/locationResponses';
import TestEnvironment from '../../TestEnvironment';
import { createZombie } from '../integrationTestUtils';

const testEnvironment = new TestEnvironment();

describe('GET /zombies Integration Test', () => {
  let app: Express;
  let locations: LocationResponse[];
  let zombieResponses: Response[];

  beforeEach(async () => {
    app = await testEnvironment.getExpressApp();
    const response = await request(app).get('/locations');
    locations = response.body.data;
  });

  afterEach(async () => {
    await testEnvironment.teardown();
  });

  it('should return a 200 status and a list of all the zombies', async () => {
    const zombie1 = await createZombie(app, {
      name: 'Zombie 1',
      locationId: locations[0].id,
    });
    const zombie2 = await createZombie(app, {
      name: 'Zombie 2',
      locationId: locations[0].id,
    });
    const zombie3 = await createZombie(app, {
      name: 'Zombie 3',
      locationId: locations[1].id,
    });

    const originalZombieList = await request(app).get('/zombies');

    const response = await request(app).delete(`/zombies/${zombie2.body.id}`);

    const latestZombieList = await request(app).get('/zombies');

    expect(response.status).toBe(204);
    const { data: originalData } = originalZombieList.body;
    expect(originalData).toEqual([zombie1.body, zombie2.body, zombie3.body]);
    expect(originalData).toHaveLength(3);

    const { data: latestData } = latestZombieList.body;
    expect(latestData).toEqual([zombie1.body, zombie3.body]);
    expect(latestData).toHaveLength(2);
  });
});
