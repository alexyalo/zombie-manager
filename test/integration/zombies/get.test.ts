import { Express } from 'express';
import request from 'supertest';
import { LocationResponse } from '../../../src/delivery/responses/locationResponses';
import TestEnvironment from '../../TestEnvironment';
import { createZombie } from '../integrationTestUtils';

const testEnvironment = new TestEnvironment();

describe('GET /zombies Integration Test', () => {
  let app: Express;
  let locations: LocationResponse[];

  beforeEach(async () => {
    app = await testEnvironment.getExpressApp();
    const response = await request(app).get('/locations');
    locations = response.body.data;

    await Promise.all([
      createZombie(app, {
        name: 'Zombie 1',
        locationId: locations[0].id,
      }),
      createZombie(app, {
        name: 'Zombie 2',
        locationId: locations[0].id,
      }),
      createZombie(app, {
        name: 'Zombie 3',
        locationId: locations[1].id,
      }),
      createZombie(app, {
        name: 'Zombie 4',
        locationId: locations[1].id,
      }),
      createZombie(app, {
        name: 'Zombie 5',
        locationId: locations[2].id,
      }),
      createZombie(app, {
        name: 'Zombie 6',
        locationId: locations[2].id,
      }),
    ]);
  });

  afterEach(async () => {
    await testEnvironment.teardown();
  });

  it('should return a 200 status and a list of all the zombies', async () => {
    const response = await request(app).get('/zombies');

    expect(response.status).toBe(200);
    const { data } = response.body;
    expect(data).toHaveLength(6);
    expect(data[0].id).toHaveLength(36);
    expect(data[0].name).toBe('Zombie 1');
    expect(data[0].location.id).toHaveLength(36);
    expect(data[0].location.name).toBe(locations[0].name);

    expect(data[5].id).toHaveLength(36);
    expect(data[5].name).toBe('Zombie 6');
    expect(data[5].location.id).toHaveLength(36);
    expect(data[5].location.name).toBe(locations[2].name);
  });

  it('should return a 200 status and a list of all the zombies in a location', async () => {
    const response = await request(app).get(`/zombies?locationId=${locations[1].id}`);

    expect(response.status).toBe(200);
    const { data } = response.body;
    expect(data).toHaveLength(2);
    expect(data[0].id).toHaveLength(36);
    expect(data[0].name).toBe('Zombie 3');
    expect(data[0].location.id).toHaveLength(36);
    expect(data[0].location.name).toBe(locations[1].name);

    expect(data[1].id).toHaveLength(36);
    expect(data[1].name).toBe('Zombie 4');
    expect(data[1].location.id).toHaveLength(36);
    expect(data[1].location.name).toBe(locations[1].name);
  });
});
