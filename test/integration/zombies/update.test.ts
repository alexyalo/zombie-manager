import { Express } from 'express';
import request from 'supertest';
import { LocationResponse } from '../../../src/delivery/responses/locationResponses';
import TestEnvironment from '../../TestEnvironment';
import { createZombie } from '../integrationTestUtils';

const testEnvironment = new TestEnvironment();

describe('PATCH /zombies/{id} Integration Test', () => {
  let app: Express;
  let locations: LocationResponse[];

  beforeEach(async () => {
    app = await testEnvironment.getExpressApp();
    const response = await request(app).get('/locations');
    locations = response.body.data;
  });

  afterEach(async () => {
    await testEnvironment.teardown();
  });

  it('should return 400 error when neither locationId or name parameters are sent', async () => {
    const zombieResponse = await createZombie(app, {
      name: 'John Zoomby',
      locationId: locations[0].id,
    });
    const body = {};
    const updateResponse = await request(app)
      .patch(`/zombies/${zombieResponse.body.id}`)
      .send(body);

    expect(updateResponse.status).toBe(400);
  });

  it('should return 200 status and the update zombie name in the response body', async () => {
    const zombieResponse = await createZombie(app, {
      name: 'John Zoomby',
      locationId: locations[0].id,
    });
    const response = await request(app)
      .patch(`/zombies/${zombieResponse.body.id}`)
      .send({ name: 'John Doe' })
      .set('Content-type', 'application/json');

    expect(response.status).toBe(200);
    const data = response.body;
    expect(data.id).toHaveLength(36);
    expect(data.name).toBe('John Doe');
    expect(data.location.id).toHaveLength(36);
    expect(data.location.name).toBe(locations[0].name);
  });

  it('should return 200 status and the updated zombie location in the response body', async () => {
    const zombieResponse = await createZombie(app, {
      name: 'John Zoomby',
      locationId: locations[0].id,
    });
    const response = await request(app)
      .patch(`/zombies/${zombieResponse.body.id}`)
      .send({ locationId: locations[1].id })
      .set('Content-type', 'application/json');

    expect(response.status).toBe(200);
    const data = response.body;
    expect(data.id).toHaveLength(36);
    expect(data.name).toBe('John Zoomby');
    expect(data.location.id).toHaveLength(36);
    expect(data.location.name).toBe(locations[1].name);
  });
});
