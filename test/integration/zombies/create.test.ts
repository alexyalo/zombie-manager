import { Express } from 'express';
import request from 'supertest';
import { LocationResponse } from '../../../src/delivery/responses/locationResponses';
import TestEnvironment from '../../TestEnvironment';

const testEnvironment = new TestEnvironment();

describe('POST /zombies Integration Test', () => {
  let app: Express;
  let locations: LocationResponse[];

  beforeEach(async () => {
    app = await testEnvironment.getExpressApp();
    const response = await request(app).get('/locations');
    locations = response.body.data;
  });

  afterEach(async () => {
    await testEnvironment.teardown();
  });

  it('should return a 400 error when missing locationId', async () => {
    const body = {
      name: 'John Zoomby',
    };
    const response = await request(app).post('/zombies').send(body);

    expect(response.status).toBe(400);
  });

  it('should return a 201 status and the created zombie in the response body', async () => {
    const body = {
      name: 'John Zoomby',
      locationId: locations[0].id,
    };
    const response = await request(app)
      .post('/zombies')
      .send(body)
      .set('Content-type', 'application/json');

    expect(response.status).toBe(201);
    const data = response.body;
    expect(data.id).toHaveLength(36);
    expect(data.name).toBe(body.name);
    expect(data.location.id).toHaveLength(36);
    expect(data.location.name).toBe(locations[0].name);
  });
});
