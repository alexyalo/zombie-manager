import request from 'supertest';
import { Express } from 'express';

export const createZombie = (app: Express, data: any) => {
  return request(app).post('/zombies').send(data).set('Content-type', 'application/json');
};
