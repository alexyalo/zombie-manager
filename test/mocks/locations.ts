import { Location } from '../../src/core/location/domain/Location';
import { UUID } from '../../src/core/common/infrastructure/UUID';
import { LocationEntity } from '../../src/core/common/infrastructure/database/entities/LocationEntity';

export const mockLocations: Location[] = [
  {
    id: new UUID('test-id-1'),
    name: 'The Hospital',
    zombieCount: 3,
  },
  {
    id: new UUID('test-id-2'),
    name: 'The School',
    zombieCount: 5,
  },
  {
    id: new UUID('test-id-3'),
    name: 'The Warehouse',
    zombieCount: 8,
  },
];

export const mockEmptyLocations: Location[] = [
  {
    id: new UUID('test-id-1'),
    name: 'The Hospital',
    zombieCount: 0,
  },
  {
    id: new UUID('test-id-2'),
    name: 'The School',
    zombieCount: 0,
  },
  {
    id: new UUID('test-id-3'),
    name: 'The Warehouse',
    zombieCount: 0,
  },
];

const getFakeLocationEntity = ({ id, name }: { id: string; name: string }): LocationEntity => {
  const entity = new LocationEntity();
  entity.id = id;
  entity.name = name;
  entity.zombies = [];
  return entity;
};

export const getMockLocationEntities = (): LocationEntity[] => {
  const hospital = getFakeLocationEntity({ id: 'test-id-1', name: 'The Hospital' });
  const school = getFakeLocationEntity({ id: 'test-id-2', name: 'The School' });
  const warehouse = getFakeLocationEntity({ id: 'test-id-3', name: 'The Warehouse' });
  return [hospital, school, warehouse];
};
