import { Zombie } from '../../src/core/zombie/domain/Zombie';
import { UUID } from '../../src/core/common/infrastructure/UUID';
import { ZombieResponse, ZombiesResponse } from '../../src/delivery/responses/zombieResponses';
import { ZombieInput } from '../../src/core/zombie/domain/ZombieInput';
import { ZombieEntity } from '../../src/core/common/infrastructure/database/entities/ZombieEntity';
import { getMockLocationEntities } from './locations';
import { idText } from 'typescript';

const getFakeZombie = ({ id, name }: { id: string; name: string }): Zombie => ({
  id: new UUID(id),
  name,
  location: {
    id: new UUID('test-id-1'),
    name: 'The Hospital',
  },
});

const getFakeZombieResponse = ({ id, name }: { id: string; name: string }): ZombieResponse => ({
  id,
  name,
  location: {
    id: 'test-id-1',
    name: 'The Hospital',
  },
});

export const mockZombie: Zombie = getFakeZombie({ id: 'test-id-1', name: 'John Doe' });

export const mockZombieList: Zombie[] = [
  getFakeZombie({ id: '1', name: 'Zombie 1' }),
  getFakeZombie({ id: '2', name: 'Zombie 2' }),
  getFakeZombie({ id: '3', name: 'Zombie 3' }),
];

export const mockZombieListResponse: ZombiesResponse = {
  data: [
    getFakeZombieResponse({ id: '1', name: 'Zombie 1' }),
    getFakeZombieResponse({ id: '2', name: 'Zombie 2' }),
    getFakeZombieResponse({ id: '3', name: 'Zombie 3' }),
  ],
};

export const mockZombieResponse: ZombieResponse = getFakeZombieResponse({ id: 'test-id-1', name: 'John Doe' });

export const mockZombieInput: ZombieInput = {
  locationId: new UUID('test-location-id'),
  name: 'John Doe',
};

export const getFakeZombieEntities = (): ZombieEntity[] => {
  const [loc] = getMockLocationEntities();

  const entity = new ZombieEntity();
  entity.id = '1';
  entity.name = 'Zombie 1';
  entity.location = loc;

  const entity2 = new ZombieEntity();
  entity2.id = '2';
  entity2.name = 'Zombie 2';
  entity2.location = loc;

  const entity3 = new ZombieEntity();
  entity3.id = '3';
  entity3.name = 'Zombie 3';
  entity3.location = loc;

  return [entity, entity2, entity3];
};

export const getFakeZombieEntity = (): ZombieEntity => {
  const entity = new ZombieEntity();
  entity.id = 'test-id-1';
  entity.name = 'John Doe';
  const [loc] = getMockLocationEntities();
  entity.location = loc;

  return entity;
};

export const getFakeZombieEntityWithNoRelations = (): ZombieEntity => {
  const entity = new ZombieEntity();
  entity.id = 'test-id-1';
  entity.name = 'John Doe';
  entity.locationId = 'test-id-1';
  return entity;
};

export const getFakeInputZombieEntity = (): ZombieEntity => {
  const entity = new ZombieEntity();
  entity.name = 'John Doe';
  const [loc] = getMockLocationEntities();
  entity.location = loc;

  return entity;
};
