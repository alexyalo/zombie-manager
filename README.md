# Zombie Manager Challenge - Express API 

We've got the zombies under control, but we need to keep track of them. That's where you come in. We need you to build an app for us. A Zombie Manager.
We can quarantine zombies in three locations: the hospital, the school, and the warehouse. We need the app to keep track of where each zombie is being held, how many zombies are in each location, and we need to be able to move zombies between the locations.
This needs to be a fully-functional app with a separate front-end and back-end. The front-end should communicate with the back-end via an API.
Please use React and submit your code in a Github repo that we can access. And keep track of how long it took you. 

# Frontend that calls this API
[https://zombie-manager-web.vercel.app/](https://zombie-manager-web.vercel.app/)

# Unit Tests
All the `core` and `delivery` layers' logic is unit tested. These unit tests can be found inside the `__tests__` folders inside the `core` and `delivery` layers.
Eg: If you want to see the unit tests for the Action `src/core/zombies/actions/CreateZombie.ts`, you will find it in `src/core/zombies/actions/__tests__/CreateZombie.test.ts`

```
$ yarn test
```

## Coverage above 90%
```
=============================== Coverage summary ===============================
Statements   : 97.24% ( 141/145 )
Branches     : 91.67% ( 33/36 )
Functions    : 93.18% ( 41/44 )
Lines        : 98.31% ( 116/118 )
================================================================================
```

# Integration Tests
I wrote Integration Test suites that hit the express API and make sure that all the endpoints work fine from top to bottom.
These tests can be found in `/tests/integration`.
The integration tests use a SQLITE database that gets cleaned and repopulated after and before each test.

```
$ yarn test-integration
```


# Start local server

## Environment variables
Before running the server, make sure you create a file named `.env` on the root of the project and add these variables to it:
```
NODE_ENV=development
TYPEORM_URL={place your mysql connection here}
TYPEORM_SYNCHRONIZE=true
TYPEORM_LOGGING=false
TYPEORM_ENTITIES=dist/core/common/infrastructure/database/entities/**/*.js
TYPEORM_MIGRATIONS=dist/core/common/infrastructure/database/migrations/**/*.js
TYPEORM_ENTITIES_DIR=src/core/common/infrastructure/database/entities/
TYPEORM_MIGRATIONS_DIR=src/core/common/infrastructure/database/migrations

PORT=8080
```

## Build and Start server
```
$ yarn start # this command will build and start the server
```

# Architecture
I applied concepts of Clean Architecture (by Uncle Bob) and Interaction Driven Design (by Sandro Mancuso).
All the core domain logic is decoupled from the Express framework and can be found inside the `core` folder.

Express is the delivery mechanism and the `core` logic knows nothing about it. 
Inside the `delivery` folder you will find all express related code, which only knows about delivery stuff.

## Core
The `core` folder architecture is grouped by "component", and inside each "component" you will find the use cases (actions), domain interfaces, and infrastructure implementations of those interfaces.

## Reference
If you wanna learn more about this type of Clean Architecture please check Sandro Mancuso's video in which he explains it: [Crafted Design by Sandro Mancuso](https://vimeo.com/128596005)

# Heroku
This API is hosted on a Heroku free container, which goes to sleep after 30 minutes of inactivity. So, the first time someone visits, the container will be awaken, taking 1 minute to fully start. After that first time, it will remain awaken during it's usage.
TLDR: First time you enter the zombie manager it will take 1 minute before the API is fully awaken, so give it a min and refresh the page.