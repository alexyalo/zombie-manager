import express from 'express';
import { createZombieMiddleware } from '../middlewares/zombies/createZombieMiddleware';
import { deleteZombieMiddleware } from '../middlewares/zombies/deleteZombieMiddleware';
import { getZombiesMiddleware } from '../middlewares/zombies/getZombiesMiddleware';
import { updateZombieMiddleware } from '../middlewares/zombies/updateZombieMiddleware';

const zombiesRouter = express.Router();

zombiesRouter.get('/zombies', getZombiesMiddleware);
zombiesRouter.post('/zombies', createZombieMiddleware);
zombiesRouter.patch('/zombies/:id', updateZombieMiddleware);
zombiesRouter.delete('/zombies/:id', deleteZombieMiddleware);

export { zombiesRouter };
