import express from 'express';
import { getLocationsMiddleware } from '../middlewares/locations/getLocationsMiddleware';

const locationsRouter = express.Router();

locationsRouter.get('/locations', getLocationsMiddleware);

export { locationsRouter };
