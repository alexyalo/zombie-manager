import { Zombie } from '../../core/zombie/domain/Zombie';
import { ZombieLocation } from '../../core/zombie/domain/ZombieLocation';

export interface ZombieLocationResponse {
  id: string;
  name: string;
}

export interface ZombieResponse {
  id: string;
  name: string;
  location: ZombieLocationResponse;
}

export interface ZombiesResponse {
  data: ZombieResponse[];
}

export const toZombieLocationResponse = (l: ZombieLocation): ZombieLocationResponse => ({
  id: l.id.getValue(),
  name: l.name,
});

export const toZombieResponse = (z: Zombie): ZombieResponse => ({
  id: z.id.getValue(),
  name: z.name,
  location: toZombieLocationResponse(z.location),
});
