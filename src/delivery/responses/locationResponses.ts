import { Location } from '../../core/location/domain/Location';

export interface LocationResponse {
  id: string;
  name: string;
  zombieCount: number;
}

export interface LocationsResponse {
  data: LocationResponse[];
}

export const toLocationResponse = (l: Location): LocationResponse => ({
  id: l.id.getValue(),
  name: l.name,
  zombieCount: l.zombieCount,
});
