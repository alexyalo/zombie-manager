import { Request, Response } from 'express';
import { UUID } from '../../../core/common/infrastructure/UUID';
import { UpdateZombieInput } from '../../../core/zombie/domain/ZombieInput';
import { InvalidInput } from '../../errors/InvalidInput';
import { toZombieResponse } from '../../responses/zombieResponses';

interface Params {
  id: string;
}

interface ZombieRequestBody {
  name?: string;
  locationId?: string;
}

const validateBody = (body: ZombieRequestBody) => {
  if (!body.name && !body.locationId) throw new InvalidInput();
};

export const updateZombieMiddleware = async (
  req: Request<Params, ZombieRequestBody>,
  res: Response,
): Promise<void> => {
  try {
    validateBody(req.body);

    const updateZombie = req.context.actions.forUpdateZombie();
    const input: UpdateZombieInput = {};
    if (req.body.name) input.name = req.body.name;
    if (req.body.locationId) input.locationId = new UUID(req.body.locationId);

    const zombie = await updateZombie.execute(new UUID(req.params.id), input);

    res.status(200).send(toZombieResponse(zombie));
  } catch (err) {
    res.status(400).send();
  }
};
