import { Request, Response } from 'express';
import { UUID } from '../../../core/common/infrastructure/UUID';

interface Params {
  id: string;
}

export const deleteZombieMiddleware = async (
  req: Request<Params>,
  res: Response,
): Promise<void> => {
  const deleteZombie = req.context.actions.forDeleteZombie();

  await deleteZombie.execute(new UUID(req.params.id));

  res.status(204).send();
};
