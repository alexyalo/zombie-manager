import { Request, Response } from 'express';
import { UUID } from '../../../core/common/infrastructure/UUID';
import { ZombieInput } from '../../../core/zombie/domain/ZombieInput';
import { InvalidInput } from '../../errors/InvalidInput';
import { toZombieResponse } from '../../responses/zombieResponses';

interface ZombieRequestBody {
  name: string;
  locationId: string;
}

const validateBody = (body: ZombieRequestBody) => {
  if (!body.name) throw new InvalidInput();
  if (!body.locationId) throw new InvalidInput();
};

export const createZombieMiddleware = async (
  req: Request<any, any, ZombieRequestBody>,
  res: Response,
): Promise<void> => {
  try {
    validateBody(req.body);
    const createZombie = req.context.actions.forCreateZombie();
    const input: ZombieInput = {
      name: req.body.name,
      locationId: new UUID(req.body.locationId),
    };
    const zombie = await createZombie.execute(input);

    res.status(201).send(toZombieResponse(zombie));
  } catch (err) {
    res.status(400).send();
  }
};
