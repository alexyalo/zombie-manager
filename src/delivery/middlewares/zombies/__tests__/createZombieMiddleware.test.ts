import { Request, Response } from 'express';
import Substitute, { SubstituteOf } from '@fluffy-spoon/substitute';
import { CreateZombie } from '../../../../core/zombie/actions/CreateZombie';
import { ActionProvider } from '../../../../dependency-injection/ActionProvider';
import Context from '../../../context';
import { createZombieMiddleware } from '../createZombieMiddleware';
import { mockZombie, mockZombieResponse } from '../../../../../test/mocks/zombies';
import { ZombieInput } from '../../../../core/zombie/domain/ZombieInput';
import { UUID } from '../../../../core/common/infrastructure/UUID';

describe('Create Zombie Middleware Unit Test', () => {
  const fakeInput: ZombieInput = {
    name: 'John Doe',
    locationId: new UUID('test-id-1'),
  };
  const getSuccessfulResponseAction = (): SubstituteOf<CreateZombie> => {
    const fakeAction = Substitute.for<CreateZombie>();
    fakeAction.execute(fakeInput).resolves(mockZombie);
    return fakeAction;
  };

  const getSuccessfulActionProvider = (): SubstituteOf<ActionProvider> => {
    const actionProvider = Substitute.for<ActionProvider>();
    actionProvider.forCreateZombie().returns(getSuccessfulResponseAction());
    return actionProvider;
  };

  const getFakeReq = (): Request => {
    const req = Substitute.for<Request>();
    const fakeContext: Context = { actions: getSuccessfulActionProvider() };
    req.context.returns(fakeContext);
    return req;
  };

  it('should return the created zombie and 201 http status', async () => {
    const req = getFakeReq();
    req.body.returns({ name: fakeInput.name, locationId: fakeInput.locationId.getValue() });
    const res = Substitute.for<Response>();
    res.status(201).returns(res);

    await createZombieMiddleware(req, res);

    res.received(1).status(201);
    res.received(1).send(mockZombieResponse);
  });

  it('should return a 400 status when name is missing from the req body', async () => {
    const req = getFakeReq();
    req.body.returns({ locationId: fakeInput.locationId.getValue() });
    const res = Substitute.for<Response>();
    res.status(400).returns(res);

    await createZombieMiddleware(req, res);

    res.received(1).status(400);
    res.received(1).send();
  });

  it('should return a 400 status when locationId is missing from the req body', async () => {
    const req = getFakeReq();
    req.body.returns({ name: 'John Doe' });
    const res = Substitute.for<Response>();
    res.status(400).returns(res);

    await createZombieMiddleware(req, res);

    res.received(1).status(400);
    res.received(1).send();
  });
});
