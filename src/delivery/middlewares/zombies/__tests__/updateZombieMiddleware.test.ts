import { Request, Response } from 'express';
import Substitute, { Arg, SubstituteOf } from '@fluffy-spoon/substitute';
import Context from '../../../context';
import { mockZombieResponse } from '../../../../../test/mocks/zombies';
import { UpdateZombieInput } from '../../../../core/zombie/domain/ZombieInput';
import { UUID } from '../../../../core/common/infrastructure/UUID';
import { updateZombieMiddleware } from '../updateZombieMiddleware';
import { UpdateZombie } from '../../../../core/zombie/actions/UpdateZombie';

interface Params {
  id: string;
}

interface ZombieRequestBody {
  name?: string;
  locationId?: string;
}

describe('Update Zombie Middleware Unit Test', () => {
  let req: SubstituteOf<Request>;
  const testIdStr = 'test-id-1';
  const testId = new UUID(testIdStr);
  const updateNameInput: UpdateZombieInput = {
    name: 'John Doe',
  };
  const updateLocationIdInput: UpdateZombieInput = {
    locationId: testId,
  };

  beforeEach(() => {
    req = Substitute.for<Request>();
    req.context.returns(({
      actions: {
        forUpdateZombie: () => {
          const fakeAction = Substitute.for<UpdateZombie>();
          fakeAction
            .execute(
              Arg.is((actual) => actual.getValue() === testIdStr),
              Arg.is((actual) => actual === updateNameInput || actual === updateLocationIdInput),
            )
            .resolves({
              id: testId,
              name: 'John Doe',
              location: {
                name: 'The Hospital',
                id: new UUID('test-id-1'),
              },
            });
          return fakeAction;
        },
      },
    } as unknown) as Context);
  });

  it('should update the zombie and return the updated object when only name is passed', async () => {
    req.body.returns(updateNameInput);
    req.params.returns({ id: 'test-id-1' });
    const res = Substitute.for<Response>();
    res.status(200).returns(res);

    await updateZombieMiddleware((req as unknown) as Request<Params, ZombieRequestBody>, res);

    res.received(1).status(200);
    res.received(1).send(mockZombieResponse);
  });

  it('should update the zombie and return the updated object when only location', async () => {
    req.body.returns(updateLocationIdInput);
    req.params.returns({ id: 'test-id-1' });
    const res = Substitute.for<Response>();
    res.status(200).returns(res);

    await updateZombieMiddleware((req as unknown) as Request<Params, ZombieRequestBody>, res);

    res.received(1).status(200);
    res.received(1).send(mockZombieResponse);
  });

  it('should return a 400 status when both name and locationId are missing from the req body', async () => {
    req.body.returns({});
    const res = Substitute.for<Response>();
    res.status(400).returns(res);

    await updateZombieMiddleware((req as unknown) as Request<Params, ZombieRequestBody>, res);

    res.received(1).status(400);
    res.received(1).send();
  });
});
