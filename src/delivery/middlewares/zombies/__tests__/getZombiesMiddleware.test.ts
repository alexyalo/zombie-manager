import { Request, Response } from 'express';
import Substitute from '@fluffy-spoon/substitute';
import Context from '../../../context';
import { getZombiesMiddleware } from '../getZombiesMiddleware';
import { GetZombies } from '../../../../core/zombie/actions/GetZombies';
import { mockZombieList, mockZombieListResponse } from '../../../../../test/mocks/zombies';
import { UUID } from '../../../../core/common/infrastructure/UUID';

interface QueryParams {
  locationId: string;
}

describe('getZombiesMiddleware Unit Test', () => {
  it('should return the zombies provided by the action', async () => {
    const req = Substitute.for<Request>();
    req.context.returns(({
      actions: {
        forGetZombies: () => {
          const fakeAction = Substitute.for<GetZombies>();
          fakeAction.execute().resolves(mockZombieList);
          return fakeAction;
        },
      },
    } as unknown) as Context);
    req.query.returns({});
    const res = Substitute.for<Response>();
    res.status(200).returns(res);

    await getZombiesMiddleware((req as unknown) as Request<any, any, any, QueryParams>, res);

    res.received(1).status(200);
    res.received(1).send(mockZombieListResponse);
  });

  it('should return the zombies provided by the action when passed a locationId as query param', async () => {
    const locationId = new UUID('test-location-id');
    const req = Substitute.for<Request>();
    req.context.returns(({
      actions: {
        forGetZombies: () => {
          const fakeAction = Substitute.for<GetZombies>();
          fakeAction.execute(locationId).resolves(mockZombieList);
          return fakeAction;
        },
      },
    } as unknown) as Context);
    req.query.returns({ locationId: locationId.getValue() });
    const res = Substitute.for<Response>();
    res.status(200).returns(res);

    await getZombiesMiddleware((req as unknown) as Request<any, any, any, QueryParams>, res);

    res.received(1).status(200);
    res.received(1).send(mockZombieListResponse);
  });
});
