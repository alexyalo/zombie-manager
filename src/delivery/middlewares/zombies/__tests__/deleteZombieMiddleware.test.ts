import { Request, Response } from 'express';
import Substitute from '@fluffy-spoon/substitute';
import Context from '../../../context';
import { UUID } from '../../../../core/common/infrastructure/UUID';
import { deleteZombieMiddleware } from '../deleteZombieMiddleware';
import { DeleteZombie } from '../../../../core/zombie/actions/DeleteZombie';

interface Params {
  id: string;
}

describe('deleteZombiesMiddleware Unit Test', () => {
  it('should invoke the action and return a 204 status code when there are no errors', async () => {
    const id = new UUID('test-id-1');
    const req = Substitute.for<Request>();
    req.context.returns(({
      actions: {
        forDeleteZombie: () => {
          const fakeAction = Substitute.for<DeleteZombie>();
          fakeAction.execute(id).resolves();
          return fakeAction;
        },
      },
    } as unknown) as Context);
    req.params.returns({ id: id.getValue() });
    const res = Substitute.for<Response>();
    res.status(204).returns(res);

    await deleteZombieMiddleware((req as unknown) as Request<Params>, res);

    res.received(1).status(204);
    res.received(1).send();
  });
});
