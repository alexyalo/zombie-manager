import { Request, Response } from 'express';
import { UUID } from '../../../core/common/infrastructure/UUID';
import { Zombie } from '../../../core/zombie/domain/Zombie';
import { toZombieResponse, ZombiesResponse } from '../../responses/zombieResponses';

interface QueryParams {
  locationId?: string;
}

const toZombiesResponse = (zombies: Zombie[]): ZombiesResponse => ({
  data: zombies.map(toZombieResponse),
});

export const getZombiesMiddleware = async (
  req: Request<any, any, any, QueryParams>,
  res: Response,
): Promise<void> => {
  const getZombies = req.context.actions.forGetZombies();
  const zombies = req.query.locationId
    ? await getZombies.execute(new UUID(req.query.locationId))
    : await getZombies.execute();

  res.status(200).send(toZombiesResponse(zombies));
};
