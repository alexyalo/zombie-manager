import { NextFunction, Request, Response } from 'express';
import Context from '../context';

type Fn = (req: Request, res: Response, next: NextFunction) => void;

const makeSetReqContext = (context: Context): Fn => {
  const setReqContext = (req: Request, res: Response, next: NextFunction) => {
    req.context = context;
    return next();
  };

  return setReqContext;
};

export default makeSetReqContext;
