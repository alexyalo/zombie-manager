import { Request, Response } from 'express';
import Substitute, { SubstituteOf } from '@fluffy-spoon/substitute';
import { getLocationsMiddleware } from '../getLocationsMiddleware';
import { LocationsResponse } from '../../../responses/locationResponses';
import { ActionProvider } from '../../../../dependency-injection/ActionProvider';
import Context from '../../../context';
import { GetLocations } from '../../../../core/location/actions/GetLocations';
import { mockLocations } from '../../../../../test/mocks/locations';

const expected: LocationsResponse = {
  data: [
    {
      id: 'test-id-1',
      name: 'The Hospital',
      zombieCount: 3,
    },
    {
      id: 'test-id-2',
      name: 'The School',
      zombieCount: 5,
    },
    {
      id: 'test-id-3',
      name: 'The Warehouse',
      zombieCount: 8,
    },
  ],
};

describe('getLocationsMiddleware Unit Test', () => {
  const getSuccessfulResponseAction = (): SubstituteOf<GetLocations> => {
    const fakeAction = Substitute.for<GetLocations>();
    fakeAction.execute().resolves(mockLocations);
    return fakeAction;
  };

  const getSuccessfulActionProvider = (): SubstituteOf<ActionProvider> => {
    const actionProvider = Substitute.for<ActionProvider>();
    actionProvider.forGetLocations().returns(getSuccessfulResponseAction());
    return actionProvider;
  };

  const getFakeReq = (): Request => {
    const req = Substitute.for<Request>();
    const actionProvider = getSuccessfulActionProvider();
    const fakeContext: Context = { actions: actionProvider };
    req.context.returns(fakeContext);
    return req;
  };

  it('should return the locations provided by the action', async () => {
    const req = getFakeReq();
    const res = Substitute.for<Response>();
    res.status(200).returns(res);

    await getLocationsMiddleware(req, res);

    res.received(1).status(200);
    res.received(1).send(expected);
  });
});
