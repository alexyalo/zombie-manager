import { Request, Response } from 'express';
import { LocationsResponse, toLocationResponse } from '../../responses/locationResponses';
import { Location } from '../../../core/location/domain/Location';

const toLocationsResponse = (locations: Location[]): LocationsResponse => ({
  data: locations.map(toLocationResponse),
});

export const getLocationsMiddleware = async (req: Request, res: Response): Promise<void> => {
  const getLocations = req.context.actions.forGetLocations();

  const locations = await getLocations.execute();

  res.status(200).send(toLocationsResponse(locations));
};
