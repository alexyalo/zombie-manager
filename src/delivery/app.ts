import express, { Express, NextFunction, Request, Response } from 'express';
import bodyParser from 'body-parser';

import Context from './context';
import makeSetReqContext from './middlewares/makeSetReqContext';
import { locationsRouter } from './routers/locations';
import { zombiesRouter } from './routers/zombies';

const setHealthzRoute = (app: Express) => app.get('/healthz', (_, res) => res.status(200).send());

const allowCors = (req: Request, res: Response, next: NextFunction) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS,PATCH');
  res.header(
    'Access-Control-Allow-Headers',
    'Content-Type, Authorization, Content-Length, X-Requested-With',
  );
  res.header('Access-Control-Allow-Credentials', 'true');

  if (req.method === 'OPTIONS') {
    res.sendStatus(200);
  } else {
    next();
  }
};

export const makeApp = (context: Context): Express => {
  const app = express();
  app.use(allowCors);

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(makeSetReqContext(context));

  setHealthzRoute(app);
  app.use(locationsRouter);
  app.use(zombiesRouter);

  return app;
};
