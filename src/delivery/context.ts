import { Connection } from 'typeorm';
import { ActionProvider } from '../dependency-injection/ActionProvider';
import { RepositoryProvider } from '../dependency-injection/RepositoryProvider';

export default interface Context {
  actions: ActionProvider;
}

export async function getContext(dbConnection: Connection): Promise<Context> {
  const repositoryProvider = new RepositoryProvider(dbConnection);

  return {
    actions: new ActionProvider(repositoryProvider),
  };
}
