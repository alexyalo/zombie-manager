import 'reflect-metadata'; // "reflect-metadata" needed for TypeORM

import { createConnection } from 'typeorm';
import { getContext } from './delivery/context';
import { makeApp } from './delivery/app';

const PORT = process.env.PORT || 8080;

async function start() {
  const dbConnection = await createConnection();
  await dbConnection.runMigrations();

  const context = await getContext(dbConnection);
  const app = makeApp(context);

  app.listen({ port: PORT }, () => console.log(`🚀 Server ready at http://localhost:${PORT}`));
}

start();
