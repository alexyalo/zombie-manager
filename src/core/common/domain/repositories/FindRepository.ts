export interface FindRepository<T, P = void> {
  find(): Promise<T>;
  find(params: P): Promise<T>;
}
