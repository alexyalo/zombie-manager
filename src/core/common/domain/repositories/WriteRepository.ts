import ID from '../ID';

export interface WriteRepository<I, T, U = I> {
  create(data: I): Promise<T>;
  update(id: ID, data: U): Promise<T>;
  delete(id: ID): Promise<void>;
}
