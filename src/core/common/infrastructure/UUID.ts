import ID from '../domain/ID';

export class UUID implements ID {
  constructor(private value: string) {}

  public getValue(): string {
    return this.value;
  }
}
