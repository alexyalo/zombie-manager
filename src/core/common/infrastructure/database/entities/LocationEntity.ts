import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Location } from '../../../../location/domain/Location';
import { UUID } from '../../UUID';
// eslint-disable-next-line import/no-cycle
import { ZombieEntity } from './ZombieEntity';

@Entity()
export class LocationEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  /* istanbul ignore next */
  @OneToMany(() => ZombieEntity, (z) => z.location)
  public zombies: ZombieEntity[];

  toDomainModel(): Location {
    return {
      id: new UUID(this.id),
      name: this.name,
      zombieCount: this.zombies?.length,
    };
  }
}
