import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Zombie } from '../../../../zombie/domain/Zombie';
import { UUID } from '../../UUID';
// eslint-disable-next-line import/no-cycle
import { LocationEntity } from './LocationEntity';

@Entity()
export class ZombieEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  public locationId: string;

  @ManyToOne(() => LocationEntity)
  public location: LocationEntity;

  toDomainModel(): Zombie {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { zombieCount, ...zombieLocation } = this.location.toDomainModel();
    return {
      id: new UUID(this.id),
      name: this.name,
      location: zombieLocation,
    };
  }
}
