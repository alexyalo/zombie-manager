import { getRepository, MigrationInterface } from 'typeorm';
import { LocationEntity } from '../entities/LocationEntity';
import { locationSeed } from '../seeds/location.seed';

export class SeedLocation1613833782258 implements MigrationInterface {
  public async up(): Promise<void> {
    const locationsRepo = getRepository(LocationEntity);
    const promises = locationSeed.map((location) => locationsRepo.save(location));
    await Promise.all(promises);
  }

  public async down(): Promise<void> {
    return Promise.resolve();
  }
}
