interface LocationSeed {
  name: string;
}

export const locationSeed: LocationSeed[] = [
  {
    name: 'The Hospital',
  },
  {
    name: 'The School',
  },
  {
    name: 'The Warehouse',
  },
];
