import Substitute from '@fluffy-spoon/substitute';
import { mockLocations } from '../../../../../test/mocks/locations';
import { LocationsRepository } from '../../domain/LocationsRepository';
import { GetLocations } from '../GetLocations';

describe('GetLocations Action Unit Test', () => {
  it('should invoke the repository and return the Location array', async () => {
    const locationsRepo = Substitute.for<LocationsRepository>();
    locationsRepo.find().resolves(mockLocations);
    const getLocations = new GetLocations(locationsRepo);

    const locations = await getLocations.execute();

    expect(locations).toStrictEqual(mockLocations);
  });
});
