import { Connection, Repository } from 'typeorm';
import { LocationEntity } from '../../common/infrastructure/database/entities/LocationEntity';
import { Location } from '../domain/Location';
import { LocationsRepository } from '../domain/LocationsRepository';

export class ORMLocationsRepository implements LocationsRepository {
  private ormRepo: Repository<LocationEntity>;

  constructor(private dbConnection: Connection) {
    this.ormRepo = dbConnection.getRepository(LocationEntity);
  }

  async find(): Promise<Location[]> {
    const entities = await this.ormRepo.find({
      order: {
        name: 'ASC',
      },
      relations: ['zombies'],
    });

    return entities.map((e) => e.toDomainModel());
  }
}
