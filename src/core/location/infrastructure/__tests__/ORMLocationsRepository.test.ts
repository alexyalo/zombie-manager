import Substitute from '@fluffy-spoon/substitute';
import { Connection, Repository } from 'typeorm';
import { getMockLocationEntities, mockEmptyLocations } from '../../../../../test/mocks/locations';
import { LocationEntity } from '../../../common/infrastructure/database/entities/LocationEntity';
import { ORMLocationsRepository } from '../ORMLocationsRepository';

describe('ORMLocationsRepository Unit Test', () => {
  describe('find method', () => {
    it('should return an array containing the locations parsed from the database entities array', async () => {
      const fakeDBConnection = Substitute.for<Connection>();
      const fakeORMRepo = Substitute.for<Repository<LocationEntity>>();
      fakeORMRepo
        .find({
          order: {
            name: 'ASC',
          },
          relations: ['zombies'],
        })
        .resolves(getMockLocationEntities());
      fakeDBConnection.getRepository(LocationEntity).returns(fakeORMRepo);
      const repo = new ORMLocationsRepository(fakeDBConnection);

      const locations = await repo.find();

      expect(locations).toStrictEqual(mockEmptyLocations);
    });
  });
});
