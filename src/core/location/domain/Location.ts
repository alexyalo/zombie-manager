import ID from '../../common/domain/ID';

export interface Location {
  id: ID;
  name: string;
  zombieCount: number;
}
