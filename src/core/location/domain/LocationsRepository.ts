import { FindRepository } from '../../common/domain/repositories/FindRepository';
import { Location } from './Location';

type FindResult = Location[];
export type LocationsRepository = FindRepository<FindResult>;
