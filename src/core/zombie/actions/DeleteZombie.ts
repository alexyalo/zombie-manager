import ID from '../../common/domain/ID';
import { ZombiesRepository } from '../domain/ZombiesRepository';

export class DeleteZombie {
  constructor(private zombiesRepo: ZombiesRepository) {}

  execute(id: ID): Promise<void> {
    return this.zombiesRepo.delete(id);
  }
}
