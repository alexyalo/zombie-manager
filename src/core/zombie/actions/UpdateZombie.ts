import ID from '../../common/domain/ID';
import { Zombie } from '../domain/Zombie';
import { UpdateZombieInput } from '../domain/ZombieInput';
import { ZombiesRepository } from '../domain/ZombiesRepository';

export class UpdateZombie {
  constructor(private zombiesRepo: ZombiesRepository) {}

  execute(id: ID, input: UpdateZombieInput): Promise<Zombie> {
    return this.zombiesRepo.update(id, input);
  }
}
