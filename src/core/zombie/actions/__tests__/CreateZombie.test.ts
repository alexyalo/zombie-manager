import Substitute from '@fluffy-spoon/substitute';
import { mockZombie, mockZombieInput } from '../../../../../test/mocks/zombies';
import { ZombiesRepository } from '../../domain/ZombiesRepository';
import { CreateZombie } from '../CreateZombie';

describe('CreateZombie Action Unit Test', () => {
  it('should invoke the repository and return the created Zombie', async () => {
    const repo = Substitute.for<ZombiesRepository>();
    const expected = mockZombie;
    const input = mockZombieInput;
    repo.create(input).resolves(expected);
    const createZombie = new CreateZombie(repo);

    const zombie = await createZombie.execute(input);

    expect(zombie).toStrictEqual(expected);
  });
});
