import Substitute from '@fluffy-spoon/substitute';
import { mockZombie, mockZombieInput } from '../../../../../test/mocks/zombies';
import { UUID } from '../../../common/infrastructure/UUID';
import { ZombiesRepository } from '../../domain/ZombiesRepository';
import { UpdateZombie } from '../UpdateZombie';

describe('UpdateZombie Action Unit Test', () => {
  it('should invoke the repository and return the created Zombie', async () => {
    const repo = Substitute.for<ZombiesRepository>();
    const expected = mockZombie;
    const input = mockZombieInput;
    const id = new UUID('test-id-1');
    repo.update(id, input).resolves(expected);
    const updateZombie = new UpdateZombie(repo);

    const zombie = await updateZombie.execute(id, input);

    expect(zombie).toStrictEqual(expected);
  });
});
