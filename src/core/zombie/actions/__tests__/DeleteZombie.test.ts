import Substitute from '@fluffy-spoon/substitute';
import { UUID } from '../../../common/infrastructure/UUID';
import { ZombiesRepository } from '../../domain/ZombiesRepository';
import { DeleteZombie } from '../DeleteZombie';

describe('DeleteZombie Action Unit Test', () => {
  it('should invoke the repository with the right parameters', async () => {
    const repo = Substitute.for<ZombiesRepository>();
    const id = new UUID('test-id-1');
    const deleteZombie = new DeleteZombie(repo);

    await deleteZombie.execute(id);

    repo.received(1).delete(id);
  });
});
