import Substitute from '@fluffy-spoon/substitute';
import { mockZombieList } from '../../../../../test/mocks/zombies';
import { UUID } from '../../../common/infrastructure/UUID';
import { ZombiesRepository } from '../../domain/ZombiesRepository';
import { GetZombies } from '../GetZombies';

describe('GetZombies Action Unit Test', () => {
  it('should invoke the repository and return the data it returns', async () => {
    const repo = Substitute.for<ZombiesRepository>();
    const expected = mockZombieList;
    repo.find().resolves(expected);
    const getZombies = new GetZombies(repo);

    const zombie = await getZombies.execute();

    expect(zombie).toStrictEqual(expected);
  });

  it('should invoke the repository and return the data it returns where a locationId is passed', async () => {
    const id = new UUID('test-id-1');
    const repo = Substitute.for<ZombiesRepository>();
    const expected = mockZombieList;
    repo.find({ locationId: id }).resolves(expected);
    const getZombies = new GetZombies(repo);

    const zombie = await getZombies.execute(id);

    expect(zombie).toStrictEqual(expected);
  });
});
