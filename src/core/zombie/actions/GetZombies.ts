import ID from '../../common/domain/ID';
import { Zombie } from '../domain/Zombie';
import { ZombiesRepository } from '../domain/ZombiesRepository';

export class GetZombies {
  constructor(private zombiesRepo: ZombiesRepository) {}

  execute(): Promise<Zombie[]>;

  execute(locationId: ID): Promise<Zombie[]>;

  execute(locationId?: ID): Promise<Zombie[]> {
    return locationId ? this.zombiesRepo.find({ locationId }) : this.zombiesRepo.find();
  }
}
