import ID from '../../common/domain/ID';

export interface ZombieInput {
  name: string;
  locationId: ID;
}

export type UpdateZombieInput = Partial<ZombieInput>;
