import ID from '../../common/domain/ID';
import { ZombieLocation } from './ZombieLocation';

export interface Zombie {
  id: ID;
  name: string;
  location: ZombieLocation;
}
