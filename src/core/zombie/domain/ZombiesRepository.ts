/* eslint-disable @typescript-eslint/indent */
import { FindRepository } from '../../common/domain/repositories/FindRepository';
import { WriteRepository } from '../../common/domain/repositories/WriteRepository';
import { Zombie } from './Zombie';
import { UpdateZombieInput, ZombieInput } from './ZombieInput';
import { ZombieParams } from './ZombieParams';

type FindResult = Zombie[];
export interface ZombiesRepository
  extends WriteRepository<ZombieInput, Zombie, UpdateZombieInput>,
    FindRepository<FindResult, ZombieParams> {}
