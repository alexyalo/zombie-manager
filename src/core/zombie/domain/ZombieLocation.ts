import { Location } from '../../location/domain/Location';

export type ZombieLocation = Omit<Location, 'zombieCount'>;
