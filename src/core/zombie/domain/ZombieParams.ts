import ID from '../../common/domain/ID';

export interface ZombieParams {
  locationId: ID;
}
