import { Connection, Repository } from 'typeorm';
import ID from '../../common/domain/ID';
import { ZombieEntity } from '../../common/infrastructure/database/entities/ZombieEntity';
import { Zombie } from '../domain/Zombie';
import { UpdateZombieInput, ZombieInput } from '../domain/ZombieInput';
import { ZombieParams } from '../domain/ZombieParams';
import { ZombiesRepository } from '../domain/ZombiesRepository';

export class ORMZombiesRepository implements ZombiesRepository {
  private ormRepo: Repository<ZombieEntity>;

  constructor(private dbConnection: Connection) {
    this.ormRepo = dbConnection.getRepository(ZombieEntity);
  }

  async find(params?: ZombieParams): Promise<Zombie[]> {
    const where = params?.locationId ? { locationId: params?.locationId.getValue() } : {};
    const entitites = await this.ormRepo.find({
      where,
      order: {
        name: 'ASC',
        locationId: 'ASC',
      },
      relations: ['location'],
    });
    return entitites.map((e) => e.toDomainModel());
  }

  async create(data: ZombieInput): Promise<Zombie> {
    const entity = new ZombieEntity();
    entity.name = data.name;
    entity.locationId = data.locationId.getValue();

    const res = await this.dbConnection.manager.save(entity);
    return this.getById(res.id);
  }

  async update(id: ID, data: UpdateZombieInput): Promise<Zombie> {
    await this.ormRepo.save({
      id: id.getValue(),
      ...(data.locationId ? { locationId: data.locationId.getValue() } : {}),
      ...(data.name ? { name: data.name } : {}),
    });

    return this.getById(id.getValue());
  }

  async delete(id: ID): Promise<void> {
    await this.ormRepo.delete(id.getValue());
  }

  private async getById(id: string): Promise<Zombie> {
    const where = {
      id,
    };
    const entity = await this.ormRepo.findOne({
      where,
      relations: ['location'],
    });
    if (!entity) throw new Error('Entity not found');

    return entity.toDomainModel();
  }
}
