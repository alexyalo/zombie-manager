import Substitute, { Arg, SubstituteOf } from '@fluffy-spoon/substitute';
import { Connection, EntityManager, Repository } from 'typeorm';
import {
  getFakeZombieEntities,
  getFakeZombieEntity,
  getFakeZombieEntityWithNoRelations,
  mockZombie,
  mockZombieInput,
  mockZombieList,
} from '../../../../../test/mocks/zombies';
import { ZombieEntity } from '../../../common/infrastructure/database/entities/ZombieEntity';
import { UUID } from '../../../common/infrastructure/UUID';
import { ORMZombiesRepository } from '../ORMZombiesRepository';

describe('ORMZombiesRepository Unit Test', () => {
  const setFakeRepo = (fakeDBConnection: SubstituteOf<Connection>) => {
    const fakeORMRepo = Substitute.for<Repository<ZombieEntity>>();
    fakeORMRepo
      .findOne({
        where: {
          id: 'test-id-1',
        },
        relations: ['location'],
      })
      .resolves(getFakeZombieEntity());
    fakeDBConnection.getRepository(ZombieEntity).returns(fakeORMRepo);
    return fakeORMRepo;
  };

  const setFakeEntityManager = (fakeDBConnection: SubstituteOf<Connection>) => {
    const fakeEntityManager = Substitute.for<EntityManager>();
    fakeEntityManager.save(Arg.any()).resolves(getFakeZombieEntityWithNoRelations());
    fakeDBConnection.manager.returns(fakeEntityManager);
  };

  describe('create method', () => {
    it('should invoke the ORM correctly and return the parsed result', async () => {
      const fakeDBConnection = Substitute.for<Connection>();
      setFakeRepo(fakeDBConnection);
      setFakeEntityManager(fakeDBConnection);
      const repo = new ORMZombiesRepository(fakeDBConnection);

      const zombie = await repo.create(mockZombieInput);

      expect(zombie).toStrictEqual(mockZombie);
    });
  });

  describe('update method', () => {
    it('should invoke the ORM correctly and return the parsed result when the input contains name', async () => {
      const fakeDBConnection = Substitute.for<Connection>();
      const fakeORMRepo = setFakeRepo(fakeDBConnection);
      const repo = new ORMZombiesRepository(fakeDBConnection);
      const id = new UUID('test-id-1');

      const zombie = await repo.update(id, { name: 'New Name' });

      fakeORMRepo.received(1).save({
        id: id.getValue(),
        name: 'New Name',
      });
      expect(zombie).toStrictEqual(mockZombie);
    });

    it('should invoke the ORM correctly and return the parsed result when the input contains locationId', async () => {
      const fakeDBConnection = Substitute.for<Connection>();
      const fakeORMRepo = setFakeRepo(fakeDBConnection);
      const repo = new ORMZombiesRepository(fakeDBConnection);
      const id = new UUID('test-id-1');

      const zombie = await repo.update(id, { locationId: new UUID('test-location-id') });

      fakeORMRepo.received(1).save({
        id: id.getValue(),
        locationId: 'test-location-id',
      });
      expect(zombie).toStrictEqual(mockZombie);
    });
  });

  describe('find method', () => {
    it('should invoke the ORM correctly and return the parsed result', async () => {
      const fakeDBConnection = Substitute.for<Connection>();
      const fakeEntities = getFakeZombieEntities();
      const fakeRepo = setFakeRepo(fakeDBConnection);
      fakeRepo
        .find({
          where: {},
          order: {
            locationId: 'ASC',
            name: 'ASC',
          },
          relations: ['location'],
        })
        .resolves(fakeEntities);
      const repo = new ORMZombiesRepository(fakeDBConnection);

      const zombies = await repo.find();

      expect(zombies).toStrictEqual(mockZombieList);
    });

    it('should invoke the ORM correctly and return the parsed result when a locationId is passed', async () => {
      const fakeDBConnection = Substitute.for<Connection>();
      const fakeEntities = getFakeZombieEntities();
      const fakeParams = {
        locationId: new UUID('test-id-1'),
      };
      const fakeRepo = setFakeRepo(fakeDBConnection);
      fakeRepo
        .find({
          where: {
            locationId: fakeParams.locationId.getValue(),
          },
          order: {
            locationId: 'ASC',
            name: 'ASC',
          },
          relations: ['location'],
        })
        .resolves(fakeEntities);
      const repo = new ORMZombiesRepository(fakeDBConnection);

      const zombies = await repo.find(fakeParams);

      expect(zombies).toStrictEqual(mockZombieList);
    });
  });

  describe('delete method', () => {
    it('should invoke the ORM correctly', async () => {
      const id = new UUID('test-id-1');
      const fakeDBConnection = Substitute.for<Connection>();
      const fakeORMRepo = setFakeRepo(fakeDBConnection);
      const repo = new ORMZombiesRepository(fakeDBConnection);

      await repo.delete(id);

      fakeORMRepo.received(1).delete(id.getValue());
    });
  });
});
