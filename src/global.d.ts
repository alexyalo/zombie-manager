import Context from './delivery/context';

declare global {
  namespace Express {
    interface Request {
      context: Context;
    }
  }
}
