import { GetLocations } from '../core/location/actions/GetLocations';
import { CreateZombie } from '../core/zombie/actions/CreateZombie';
import { DeleteZombie } from '../core/zombie/actions/DeleteZombie';
import { GetZombies } from '../core/zombie/actions/GetZombies';
import { UpdateZombie } from '../core/zombie/actions/UpdateZombie';
import { RepositoryProvider } from './RepositoryProvider';

export class ActionProvider {
  constructor(private repoProvider: RepositoryProvider) {}

  forGetLocations(): GetLocations {
    return new GetLocations(this.repoProvider.getLocationsRepository());
  }

  forGetZombies(): GetZombies {
    return new GetZombies(this.repoProvider.getZombiesRepository());
  }

  forCreateZombie(): CreateZombie {
    return new CreateZombie(this.repoProvider.getZombiesRepository());
  }

  forUpdateZombie(): UpdateZombie {
    return new UpdateZombie(this.repoProvider.getZombiesRepository());
  }

  forDeleteZombie(): DeleteZombie {
    return new DeleteZombie(this.repoProvider.getZombiesRepository());
  }
}
