import { Connection } from 'typeorm';
import { LocationsRepository } from '../core/location/domain/LocationsRepository';
import { ORMLocationsRepository } from '../core/location/infrastructure/ORMLocationsRepository';
import { ZombiesRepository } from '../core/zombie/domain/ZombiesRepository';
import { ORMZombiesRepository } from '../core/zombie/infrastructure/ORMZombiesRepository';

export class RepositoryProvider {
  constructor(private dbConnection: Connection) {}

  getZombiesRepository(): ZombiesRepository {
    return new ORMZombiesRepository(this.dbConnection);
  }

  getLocationsRepository(): LocationsRepository {
    return new ORMLocationsRepository(this.dbConnection);
  }
}
